class Task < ActiveRecord::Base
  # Model for task
  belongs_to :user
  validates :description, presence: true, length: { minimum: 10, maximum: 300 }
  validates :user_id, presence: true
  validates :deadline_cannot_be_in_the_past, presence: true

  def deadline_cannot_be_in_the_past
    if deadline.present? && deadline < Date.today
      errors.add(:deadline, "can't be in the past")
    end
  end
  enum status: [:created, :inprogress, :completed]
end
