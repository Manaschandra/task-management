class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]
  before_action :require_user, only: [:new, :edit, :create, :update, :destroy]
  before_action :require_same_user, only: [:edit,  :update, :destroy]

  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = Task.paginate(page: params[:page])
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = Task.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = Task.new(task_params)

    @task.user = User.find(task_params['user_id']) if task_params['user_id']
    if @task.save
      flash[:success] = 'Task was successfully created.'
      redirect_to task_path(@task)

    else
      render 'new'
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    if @task.update(task_params)
      flash[:notice] = 'Task was successfully updated.'
      redirect_to task_path(@task)
    else
      render 'edit'
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy

    flash[:danger] = 'Task was successfully destroyed.'
    redirect_to tasks_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_task
    @task = Task.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def task_params
    params.require(:task).permit(:description, :user_id, :status, :deadline)
  end

  def require_same_user
    if current_user != @task.user && !current_user.admin?
      flash[:danger] = 'You can edit or delete your own tasks'
      redirect_to root_path
    end
  end
end
